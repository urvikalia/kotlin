package com.thoughtworks.learnkotlin

/*
Interfaces are public by default

 */
interface Time{
    fun setTime(hours:Int, mins:Int=0,seconds:Int=0)
    fun getCurrentTime():Long

}


class YetiTime: Time{
    override fun setTime(hours:Int, mins:Int, seconds:Int) {}
    override fun getCurrentTime():Long { return 0}

}

