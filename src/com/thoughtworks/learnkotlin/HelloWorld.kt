package com.thoughtworks.learnkotlin


/* Entry point to kotlin program is a function named "main
    The function is passed with array containing the command line arguments
*/
fun main(args: Array<String>)
{
    println("Hello World")

    // declaring values is done using either var or Val
    // val cannot be reassigned
    // var can be reassigned

    val x=10
    var y=20
    y=21
    println(x)
    println(y)

    //kotlin can determine the datatype, in case you need to specify the datatype the syntax is as below
    val foo:Int=7


    //String is represented in same way as in java

    var fooString ="Hello Kotlin"
    println("fooString " + fooString)

    /* A raw string is delimited by a triple quote
        Raw string can contain new line and other charchter s
    */
    val str="""
        fun print()
        {
            sout("Say hello")
        }
    """.trimIndent()

    println(str)


    /*
    A string can contain template expressions
    A template expression starts with dollar symbol
     */

    val fooStringTemplate ="$fooString has ${fooString.length} number of charchters"
    println(fooStringTemplate)

    /*
    For a variable to hold null it must be explicitly specified as nullable
    A variable can be specificed as nullable by appending a ? to its type
    We can access a nullable variable by using the ?. operator
    We can use the ?. operator to specify an alternative value to use if a variable is null
     */

    println("Variable which can hold null")
    var nullString:String? = "abc"
    println(nullString?.length?:-1)
    nullString=null
    println(nullString?.length?:-99)


    //Functions
    /*
    Functions can be declared using keyword fun
    Functions parameters/ arguments  are declared in brackets after the function name within round brackets()
    Function parameters/arguments can have default values
    Function return type can be specified after the arguments
     */

    fun hello(name:String="world"):String{
        return ("hello $name !!")
    }

    println(hello("Urvi"))
    println(hello())


    //varargs
    // function to add all the numbers in the list
    fun addAll(vararg lst:Int):Int{
        var sum:Int=0;
        for (num in lst)
            sum=sum+num
        return sum

    }

    println("sum of first 10 numbers is: " + addAll(1,2,3,4,5,6,7,8,9,10))
    println("sum of first five numbers is: " + addAll(1,2,3,4,5))
    println("sum of no numbers is: "+ addAll())


    // single line functions

    fun odd(x:Int):Boolean= x%2 ==1

    fun even(x:Int):Boolean=x%2==0

    println("Is 10 odd: "+ odd(10))
    println("Is 10 even: "+ even(10))

    // functions can take functions as arguments

    fun addTwo(x:Int):Int=x+2
    fun addOne(x:Int):Int=x+1

    fun add(option:Int=0,value:Int):Int{
        if(option==1)
            return addOne(value);
        else if(option==2)
            return addTwo(value)
        return value
    }

    println(add(value = 10))
    println(add(1,10))
    println(add(2,10))

    // functions can also return functions

    //lambda functions
    val notPositive= {n:Int-> n<0}
    println(notPositive(-1))
    println(notPositive(1))

    // function being passed and retuned
    fun not(f:(Int)-> Boolean):(Int)->Boolean{
        return {n-> !f.invoke(n)}
    }

    //Class
    //In kotlin , class is declared using class keyword
    class SampleClass(val num:Int){
        fun displayNum(){
            println("Inside sample class displayNum function: "+num)
        }

        infix fun addOne(x:Int):Int{
            return x+1
        }
    }

    //To create a new instance of class ,we call the constructor
    // Note in kotlin , we dont have a new keyworld

    var sampleObj=SampleClass(18)
    println(sampleObj.displayNum())

    // infix
    sampleObj.addOne(5)
    val addition=sampleObj addOne 20
    println("Infix style addOne result: "+ addition)


    // data classes are a concise way to create classes that just holds data
    data class Point(val x:Int,val y:Int)

    val point1=Point(2,4)
    val point2=Point(5,5)

    fun addPoints(point1:Point,point2:Point):Point{
        var point3=Point(point1.x+point2.x,point1.y+point2.y)
        return point3
    }

    println("Addition of points: ")
    println(addPoints(point1,point2))

    // data classes have a copy function
    val point1copy=point1.copy(y=100)
    println(point1copy)

    // objects can be destrutured into multiple varibales
    println("Objects can be destrutured into multiple variables ")
    val (xvar,yvar)=point2
    println(xvar)
    println(yvar)

    //destructuring in for loop
    println("destructuring in for loop")
    for((a,b) in listOf(point1,point2))
    {
        println(a)
        println(b)
    }

    println("destructuring in loop for map")
    for((key,value) in mapOf("a" to 1, "b" to 2,"c" to 3))
    {
        println(key +" -> "+ value)
    }

    //with function is similar to javascript's " with statement

    println("With function ")
    data class DemoDataClass(var num:Int)
    var demodataObj=DemoDataClass(100)
    with(demodataObj){
        num=num /2
    }
    println("Modified value in with function: "+ demodataObj.num)


    /*
        We can create a list using listOf function
        The list is immutable - elements can not be added or removed
     */

    val fruitList= listOf("apple","orange","pineapple")
    fruitList.get(0);
    println("last fruit is: "+ fruitList.last())
    println("List contains :")
    fruitList.stream().forEach({x-> println(x)})

    /*
     A mutable list can be created using "mutableListOf" function
     */
    val mutableList= mutableListOf("a","e","i","o")
    mutableList.add("u")
    mutableList.stream().forEach({x-> println(x)})

    // we can create a set of using setOf function
    println("Using setOf")
    val dataset= setOf(1,2,3,4,5,1)

    //can create a map using "mapof" function

    /*
        Sequences represent lazily evaluated collection
        we can create a sequence using generatesequence function
     */

        println("Creating a list from sequenceGenerator")
        val sequenceTemp= generateSequence ( 1,{it+1} )
        val lst=sequenceTemp.take(10).toList()

        lst.stream().forEach(::println)

    /*
      Example to generate fibonacci series using a sequence to generate fibonacci numbers
     */

    fun finbonacciSeries():Sequence<Long>{

        var a=0L;
        var b=1L;

        fun next():Long{
            var result=a+b
            a=b
            b=result
            return a
        }

        return generateSequence(::next)
    }

    fun displayWithLambda(funct:(s:String)->Unit){
        funct(str)
    }

    val fibSeries= finbonacciSeries().take(10).toList();
    println("Fibnonacci series for first 10 numbers:")
    fibSeries.stream().forEach(::println)









}


