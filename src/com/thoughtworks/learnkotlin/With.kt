package com.thoughtworks.learnkotlin

class Data(var x:Int,var y:Int,var z:Int){
    fun print()
    {
        println("Values are: " + x + " " + y + " " + z )
    }
}

fun main() {

    var d1=Data(10,15,20)

    with(d1)
    {
        x=x-5
        y=y-10
        z=z-10
    }

    println("data is: " + d1.x + " " + d1.y + " " + d1.z )

    d1.apply { x++
        y++
        z++
        }.print()

}