package com.thoughtworks.learnkotlin

class Person{
    companion object{
        @JvmStatic
        fun main(args: Array<String>) {
            println("Inside main method of static class , using companion objects")
            println("Companion objects are the way to associate static methods to a class ")
        }
    }
}