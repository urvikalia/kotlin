package com.thoughtworks.learnkotlin

fun main() {

    var number= Numbers(10)
    var printStatergy=PrintStatergy()
    number.loopthrough(printStatergy)

    // calling function loopup which has another function as parameter
    var number5 =Numbers(5)
    number5.loopup { num -> println(num) }
    number5.loopup (){ num -> print(num) }


}


interface Statergy{
    fun execute(num: Int)
}

class PrintStatergy:Statergy{
    override fun execute(num: Int) {
        println(num)
    }

}

class Numbers(val count:Int){

    fun loopthrough(statergy: Statergy){
        for (i in 1..count)
        {
            statergy.execute(i)
        }
    }


    // Instead of using statergy pattern can pass function as parameter
    fun loopup(func:(Int) -> Unit)
    {
        for(i in 1..count) {
            func(i)
        }
    }

}