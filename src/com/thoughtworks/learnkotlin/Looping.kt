package com.thoughtworks.learnkotlin

fun main() {

    //Looping mechanism in java
    // inclusive range
    print("for(i in 1..10)  ")
    for(i in 1..10)
    {

        print(i)
    }

    println()
    print("for(i in 10 downTo 1 step 2)  ")
    for(i in 10 downTo 1 step 2)
    {
        print(i)
    }
    // half loop inclusion
    println()
    print("for(i in 1 until 10) ")
    for(i in 1 until 10)
    {
        print(i)
    }

    // Looping through map
    var ageMap =HashMap<String,Int>()
    ageMap["urvi"]=37
    ageMap["vipul"]=33
    ageMap["anshul"]=28

    println()
    for((name,age) in ageMap)
    {
        println("$name: $age")
    }

    // Kotlin supports unchecked exception , you dont have to explicity handle checked exceptions


}