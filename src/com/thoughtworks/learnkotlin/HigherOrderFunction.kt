package com.thoughtworks.learnkotlin

val action ={ println("Inside action")}
val calculator ={x:Int,y:Int -> println(x+y)}

fun dosomething(func: () -> Unit){
    func()
}

fun main() {

    dosomething(action)

}