package com.thoughtworks.learnkotlin

sealed class PersonEvent{
    class Awake:PersonEvent()
    class Sleeping:PersonEvent()
    class Eating(val food:String):PersonEvent()
}

fun handlePersonEvent(event:PersonEvent)
{
    when(event)
    {
        is PersonEvent.Awake -> println("Awake")
        is PersonEvent.Sleeping -> println("Sleeping")
        is PersonEvent.Eating -> println("Eating "+ event.food)
    }
}


fun checkSealedClasses() {
    val sleeper = PersonEvent.Sleeping()
    val eater = PersonEvent.Eating("banana")
    val awake = PersonEvent.Awake()

    handlePersonEvent(sleeper)
    handlePersonEvent(eater)
    handlePersonEvent(awake)
}


fun main() {
    println("Understanding Sealed classes")
    checkSealedClasses()
}
