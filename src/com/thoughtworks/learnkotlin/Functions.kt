package com.thoughtworks.learnkotlin

fun main() {
    println("Functions in Kotlin!!")


    println("""
        In Kotlin : 
        1. Functions can be standalone , dont have to be part of a class. 
        2. Are declared using keyword - fun
        3. Can have default parameters
        4. Can have named parameters
        5. Can extend existing types 
    """)

    println("""
       Kotlin supports Extension function 
       Can add functions to the class not owned by you 
        Kotlin generates static functions 
        Benefits of having extension functions :
                Cuts down on use of utility classes
                Makes code easier to read 
    """)

    var str="hello everyone!! "
    println(str.replaceUpper())


    println("""
        Kotlin supports infix function 
        syntax is different it more looks like a operator 
        No need to use . dot and braces 
        
    """.trimIndent())

}

fun String.replaceUpper():String{
    return  this.toUpperCase();
}