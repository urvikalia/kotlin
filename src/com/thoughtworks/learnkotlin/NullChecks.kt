package com.thoughtworks.learnkotlin

import java.lang.NumberFormatException
import kotlin.reflect.jvm.internal.impl.resolve.constants.IntValue

fun main() {
    var capQuestion=Question("What is india's capital","Delhi")
    capQuestion.answer="Delhi"



    if(capQuestion.answer== capQuestion.correctAnswer)
    {
        capQuestion.getAnswer()
        println("Your answer is correct!!")
    }
    else
    {
        capQuestion.getCorrectAnswer()
        println("Try again!!")
    }

    println("In kotlin if statements are expressions then can return values themselves")
    val message = if(capQuestion.answer==capQuestion.correctAnswer)
    {
        "Your answer is correct!! "
    }
    else
    {
        "Try again!!"
    }

    println(message)

    var nullQuestion:Question?=null;
    // once you have marked a particular object as null , when using it , compiler will force you to use the null safety mechanism
    println("Question is: ${nullQuestion?.question}")
    println("answer is: ${nullQuestion?.answer}")
    println("correct answer is: ${nullQuestion?.correctAnswer}")

    // kotlin doesn't have a switch statement , instead it has a when statement
    capQuestion.printResult()

    //like if statement , try is also an expression in Kotlin

    var str:String="43"
    var intValue:Int?= try{Integer.parseInt(str)}
    catch(e: NumberFormatException) {
        null
    }

    println("output from parseInt is: $intValue")




}



class Question(var question:String,var correctAnswer:String){
    var answer:String? =""

    fun getAnswer(){
        println("you answered : $answer")
    }

    fun getCorrectAnswer(){
        println("you answered: $answer but the correct answer is $correctAnswer")
    }

    fun printResult() = when (answer) {
        correctAnswer -> println("Inside printresult: correct answer ")
        else -> println("Inside printresult: Incorrect answer")
    }

}
